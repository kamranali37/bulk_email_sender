import os
import sys
import traceback
from distutils.util import strtobool

import structlog

from utils import get_config

structlog.configure(
    processors=[
        structlog.stdlib.add_log_level,
        structlog.processors.StackInfoRenderer(),
        structlog.processors.format_exc_info,
        structlog.processors.TimeStamper(),
        structlog.processors.JSONRenderer(),
    ],
    context_class=dict,
    cache_logger_on_first_use=True,
)

logger = structlog.get_logger()


def my_except_hook(exctype, value, tb):
    logger.exception(
        value, traceback=repr(traceback.format_exception(exctype, value, tb))
    )


class Config:

    BASE_DIR = os.path.dirname(__file__)

    #Gmail Settings
    EMAIL_USE_TLS = True
    EMAIL_HOST = 'smtp.gmail.com'
    EMAIL_HOST_USER = '' # Gmail address goes here
    EMAIL_HOST_PASSWORD = '' # Gmail password goes here
    EMAIL_PORT = 465


    # Debugging
    DEBUG = strtobool(get_config("DEBUG", "False"))
    LOCAL = strtobool(get_config("LOCAL", "False"))

    # REDIS
    REDIS_HOST = get_config("REDIS_POD_HOST", "localhost")
    REDIS_PORT = int(get_config("REDIS_POD_PORT", "6379"))

    LOGGER = logger

    if not DEBUG:
        sys.excepthook = my_except_hook
