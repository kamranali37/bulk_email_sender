import smtplib
import time
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import dramatiq
from dramatiq.brokers.redis import RedisBroker
from dramatiq_abort import Abortable, abort
from dramatiq_abort.backends import RedisBackend
from dramatiq_abort.middleware import Abort

from config import Config

redis_broker = RedisBroker(host=Config.REDIS_HOST, port=Config.REDIS_PORT)
dramatiq.set_broker(redis_broker)

abortable = Abortable(backend=RedisBackend(client=redis_broker.client))
redis_broker.add_middleware(abortable)
redis_broker.add_middleware(dramatiq.middleware.CurrentMessage())


@dramatiq.actor
def print_result(message_data, result):
    print(f"The result of task {message_data['message_id']} was: {result}.")

@dramatiq.actor
def print_error(message_data, exception_data):
    print(f"Message {message_data['message_id']} failed:")
    print(f"  * type: {exception_data['type']}")
    print(f"  * message: {exception_data['message']!r}")

@dramatiq.actor
def send_email_html(recipient):
    """Send email to the given recipient

    Arguments:
        recipient {str} -- email address of the recipient

    Returns:
        str -- Success message if email is sent
    
    Throws:
        exception -- throws and logs any exception that happens in the email sending process.
    """
    if not recipient:
        Config.LOGGER.error("Recipient Email is not provided.")
        return
    try:
        # I put the sleep for 0.2 seconds to give mail server some time in between emails so it wont get over loaded. 
        time.sleep(0.2)
        # Create message container - the correct MIME type is multipart/alternative.
        msg = MIMEMultipart('alternative')
        msg['Subject'] = "D4L Sample Email"
        msg['From'] = Config.EMAIL_HOST_USER
        msg['To'] = recipient

        # Create the body of the message (a plain-text and an HTML version).
        text = "Hi!\nHow are you?\nHere is the link you wanted:\nhttp://www.python.org"
        html = f"""\
        <html>
        <head></head>
        <body>
            <p>Hi D4L<br>
            <br>
            This is just a test email in HTML, its hardcoded offcourse but same app can be modified a little bit for customized emails with HTML template and styling.<br>
            <br>
            <br>
            Best Regards
            <br>
            Kamran
            </p>
        </body>
        </html>
        """

        # Recording and attaching MIME parts to the email
        part1 = MIMEText(text, 'plain')
        part2 = MIMEText(html, 'html')
        msg.attach(part1)
        msg.attach(part2)

        # Authenticating and sending email
        server = smtplib.SMTP_SSL(Config.EMAIL_HOST, Config.EMAIL_PORT)
        server.ehlo()
        server.login(Config.EMAIL_HOST_USER, Config.EMAIL_HOST_PASSWORD)
        server.sendmail(Config.EMAIL_HOST_USER, recipient, msg.as_string())
        server.close()

        return f"Email Sent to {recipient}"

    except Exception as ex:
        Config.LOGGER.error(f"Email Sending failed: {ex}")

@dramatiq.actor
def fake_email_send():
    time.sleep(0.5)
    return "Email Sent"
