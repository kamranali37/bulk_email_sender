# Python Email Scheduler

**Setting up for development**

- [Email Scheduler](#email_scheduler)
	- [Getting Started](#getting-started)
		- [Prerequisites](#prerequisites)
		- [Installing](#installing)
        - [Notes](#notes)

		
## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.


### Prerequisites

* [Docker](https://www.docker.com/) 
* [Python 3.7](https://www.python.org/) 


### Installing

* After Downloading the repository, its recommended to create a virtual environment. To create a venv with Conda run `conda create --name=python_email_scheduler python=3.7` then run `conda activate python_email_scheduler` to activate the virtual environment 
* Change directory to bulk_email_sender and then Run `pip install -r requirements.txt` to install all the dependencies
* Then Run `docker run -d -p 6379:6379 redis` to start redis server on port 6379, if u use a different port then please update port in config.py file. 
* Then run `dramatiq scheduler` to start scheduler listening on for tasks
* In new terminal run `python send.py` to start sending the email to scheduler which will then send emails one by one asynchronously. 


### Notes
* This program is written to use gmail as default email server but a local smtp server can also be configured such as https://www.hmailserver.com/, please update settings in config.py file. 
* Please update your own gmail account name and password in config.py file to get emails working from your gmail account. Also, please allow less secure apps (https://myaccount.google.com/lesssecureapps?pli=1) in gmail account to get smtp working otherwise google will not authenticate it properly. 
* In send.py there are multiple options to use scheduler in three different ways: send one email to a specific address, fake all email sending, send real emails to a real list of addresses. Please comment or uncomment which method you want to run. Options are marked as 1,2,3 in comments. NOTE: Faking email works bit differently in scheduler because of threading and multiple workers, so time sleep for 0.5 seconds is not as effective as on main thread, solution could be to make a random task in the scheduler like run a simple loop to do something. But I kept it simple, as real email sending is working. 
* Email design was setup in HTML so there are possibilities to design a nice looking html template and send it to user, I used a very basic html email in this program for demo purposes. 
* **NOTE** : This program was developed and tested on MacOS Catalina, and was never tested on Windows and Linux, but I guess it should work on all platforms without any modifications. 
