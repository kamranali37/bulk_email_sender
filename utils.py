"""General utility functions.
Functions are used on initialization, so do not import code from this repository.
"""

import os


def get_config(key, default=None, required=False):
    """Read environment variable (with error handling).

    Arguments:
        key {str} -- name of environment variable

    Keyword Arguments:
        default {str} -- default value if the variable does not exist (default: {None})
        required {bool} -- if set to true will raise an exception,
                           if the variable does not exist (default: {False})

    Raises:
        Exception: Raised if variable does not exist and required is set to true

    Returns:
        str -- value of environment variable
    """
    value = os.getenv(key, default)
    if required and not value:
        raise Exception(f"Environment variable {key} is required but not set.")
    return value
