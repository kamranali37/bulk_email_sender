import csv
import time

from scheduler import print_error, print_result, send_email_html, fake_email_send


def get_email_list(filename=None):
    """Gets the email list from a file

    Arguments:
        filename {str} -- full name of the CSV file containing email addresses

    Returns:
        list -- List of email addresses in the CSV file
    """

    if not filename:
        filename = "MOCK_DATA.csv"
    print(filename)
    emails = []
    with open(filename, newline='') as csvfile:
        data = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for email in data:
            emails.append(email)
            # print(', '.join(email))
    return emails


if __name__ == "__main__":
    # OPTION 1: To send emails to all the recipients in the CSV file
    #for item in get_email_list():
    #    send_email_html.send_with_options(
    #    args=(item,),
    #    on_failure=print_error,
    #    on_success=print_result,
    #)


    # OPTION 2: To send only one email to specific recipient
    recipient = "kamran.ali37@hotmail.com"
    send_email_html.send_with_options(
        args=(recipient,),
        on_failure=print_error,
        on_success=print_result,
    )



    # OPTION 3: To fake email sending process, this will just make process wait for 0.5 seconds for every email entry in the list
    #or item in get_email_list():
    #   fake_email_send.send()